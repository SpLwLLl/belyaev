using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin2 : MonoBehaviour
{
    public GameObject coin;
    public Transform SpawnPoint;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(coin, SpawnPoint.position, Quaternion.identity);
        }
    }
}
