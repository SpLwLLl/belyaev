using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class play : MonoBehaviour
{
    public float Coin = 0;
    public TMP_Text CoinText;
    // Start is called before the first frame update
    void Start()
    {
        CoinText.text = ((int)Coin).ToString();
    }

    // Update is called once per frame
    void UpdateText()
    {
        CoinText.text = ((int)Coin).ToString();
    }
    void FixedUpdate()
    {
        UpdateText();
    }
    private void OnTriggerEnter(Collider Collision)
    {
        if (Collision.gameObject.tag == "Coin")
            Coin += 1;


    }






}
